$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);


	$('nav ul li a').hover( function(){
		if(!$(this).hasClass('ativo'))
			$(this).parent('li').addClass('parent-ativo');
	}, function(){
		if(!$(this).hasClass('ativo'))
			$(this).parent('li').removeClass('parent-ativo');
	});

	$('#coment-form').submit( function(){

		if($("input[name='nome']").val() == '' || $("input[name='nome']").val() == 'nome'){
			alert('Informe seu nome!');
			return false;
		}

		if($("input[name='email']").val() == '' || $("input[name='email']").val() == 'e-mail'){
			alert('Informe seu e-mail!');
			return false;
		}

		if($("textarea[name='mensagem']").val() == '' || $("textarea[name='mensagem']").val() == 'mensagem'){
			alert('Informe seu recado!');
			return false;
		}

	});

	$('#form-contato').submit( function(){

		if($("input[name='nome']").val() == '' || $("input[name='nome']").val() == 'nome'){
			alert('Informe seu nome!');
			return false;
		}

		if($("input[name='email']").val() == '' || $("input[name='email']").val() == 'e-mail'){
			alert('Informe seu e-mail!');
			return false;
		}

		if($("input[name='telefone']").val() == '' || $("input[name='telefone']").val() == 'telefone'){
			alert('Informe seu telefone de contato!');
			return false;
		}

		if($("textarea[name='mensagem']").val() == '' || $("textarea[name='mensagem']").val() == 'mensagem'){
			alert('Informe seu recado!');
			return false;
		}

	});	

	$('.fancy').fancybox();

});