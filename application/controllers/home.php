<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

   		$this->load->model('blog_model', 'blog');
    }

    function index($pag = 0){

   		$this->load->library('pagination');

      	$pag_options = array(
        	'base_url' => base_url("home/index/"),
        	'per_page' => 5,
         	'uri_segment' => 3,
         	'next_link' => "posts mais antigos &raquo;",
         	'next_tag_open' => "<div class='next'>",
         	'next_tag_close' => '</div>',
         	'prev_link' => "&laquo; posts recentes",
         	'prev_tag_open' => "<div class='prev'>",
         	'prev_tag_close' => '</div>',
         	'display_pages' => FALSE,
         	'first_link' => FALSE,
         	'last_link' => FALSE,
         	'total_rows' => $this->blog->numeroResultados()
      	);

      	$this->pagination->initialize($pag_options);
      	$data['paginacao'] = $this->pagination->create_links();

      	$data['registros'] = $this->blog->pegarPaginado($pag_options['per_page'], $pag);

      	//Pegar imagens, número de comentários, nome categoria e formatar data
      	foreach ($data['registros'] as $key => $value) {
      		$value->imagens = $this->blog->imagens($value->id);
      		$value->num_comentarios = $this->blog->num_comentarios($value->id);
      		$arre = $this->blog->categorias($value->id_blog_categorias);
      		$value->titulo_categoria = (isset($arre)) ? $arre->titulo : "Categoria não encontrada";
      		list($ano, $mes, $dia) = explode('-', $value->data);
      		$value->data_formatada = "<span class='dia'>$dia</span><br><span class='mes'>".mes($mes)."</span><br><span class='ano'>$ano</span>";
      	}

        $this->menuvar['categorias'] = $this->blog->navegacao_categorias();
        $this->menuvar['anos'] = $this->blog->navegacao_anos();

   		$this->load->view('home', $data);
    }

    function categoria($slug = '', $pag = 0){
        if(!$slug)
            redirect('home');

        $query_categoria = $this->db->get_where('blog_categorias', array('slug' => $slug))->result();

        if(!isset($query_categoria[0]))
            redirect('home');

        $result_categoria = $query_categoria[0];

        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("home/categoria/$slug/"),
            'per_page' => 5,
            'uri_segment' => 4,
            'next_link' => "posts mais antigos &raquo;",
            'next_tag_open' => "<div class='next'>",
            'next_tag_close' => '</div>',
            'prev_link' => "&laquo; posts recentes",
            'prev_tag_open' => "<div class='prev'>",
            'prev_tag_close' => '</div>',
            'display_pages' => FALSE,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'total_rows' => $this->db->get_where('blog', array('id_blog_categorias' => $result_categoria->id))->num_rows()
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->blog->pegarPaginadoPorCategoria($pag_options['per_page'], $pag, 'data', 'desc', $result_categoria->id);

        //Pegar imagens, número de comentários, nome categoria e formatar data
        foreach ($data['registros'] as $key => $value) {
            $value->imagens = $this->blog->imagens($value->id);
            $value->num_comentarios = $this->blog->num_comentarios($value->id);
            $arre = $this->blog->categorias($value->id_blog_categorias);
            $value->titulo_categoria = (isset($arre)) ? $arre->titulo : "Categoria não encontrada";
            list($ano, $mes, $dia) = explode('-', $value->data);
            $value->data_formatada = "<span class='dia'>$dia</span><br><span class='mes'>".mes($mes)."</span><br><span class='ano'>$ano</span>";
        }

        $this->menuvar['categorias'] = $this->blog->navegacao_categorias($slug);
        $this->menuvar['anos'] = $this->blog->navegacao_anos();

        $this->load->view('home', $data);   
    }

    function ano($ano = '', $pag = 0){
        if(!$ano)
            redirect('home');

        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("home/ano/$ano/"),
            'per_page' => 5,
            'uri_segment' => 4,
            'next_link' => "posts mais antigos &raquo;",
            'next_tag_open' => "<div class='next'>",
            'next_tag_close' => '</div>',
            'prev_link' => "&laquo; posts recentes",
            'prev_tag_open' => "<div class='prev'>",
            'prev_tag_close' => '</div>',
            'display_pages' => FALSE,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'total_rows' => $this->db->get_where('blog', array('YEAR(data)' => $ano))->num_rows()
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->blog->pegarPaginadoPorAno($pag_options['per_page'], $pag, 'data', 'desc', $ano);

        //Pegar imagens, número de comentários, nome categoria e formatar data
        foreach ($data['registros'] as $key => $value) {
            $value->imagens = $this->blog->imagens($value->id);
            $value->num_comentarios = $this->blog->num_comentarios($value->id);
            $arre = $this->blog->categorias($value->id_blog_categorias);
            $value->titulo_categoria = (isset($arre)) ? $arre->titulo : "Categoria não encontrada";
            list($ano, $mes, $dia) = explode('-', $value->data);
            $value->data_formatada = "<span class='dia'>$dia</span><br><span class='mes'>".mes($mes)."</span><br><span class='ano'>$ano</span>";
        }

        $this->menuvar['categorias'] = $this->blog->navegacao_categorias();
        $this->menuvar['anos'] = $this->blog->navegacao_anos($ano);

        $this->load->view('home', $data); 
    }

    function detalhes($slug = ''){

        if(!$slug)
            redirect('home');

        $query_post = $this->db->get_where('blog', array('slug' => $slug))->result();

        if (isset($query_post[0])) {
            $data['post'] = $query_post;
        }else{
            redirect('home');
        }

        foreach ($data['post'] as $key => $value) {
            $value->imagens = $this->blog->imagens($value->id);
            $value->num_comentarios = $this->db->get_where('blog_comentarios', array('id_blog' => $value->id))->num_rows();
            $arre = $this->blog->categorias($value->id_blog_categorias);
            $value->titulo_categoria = (isset($arre)) ? $arre->titulo : "Categoria não encontrada";
            list($ano, $mes, $dia) = explode('-', $value->data);
            $value->data_formatada = "<span class='dia'>$dia</span><br><span class='mes'>".mes($mes)."</span><br><span class='ano'>$ano</span>";
            $value->comentarios = $this->db->get_where('blog_comentarios', array('id_blog' => $value->id))->result();
        }        

        $data['post'] = array_shift($query_post);

        $this->menuvar['categorias'] = $this->blog->navegacao_categorias();
        $this->menuvar['anos'] = $this->blog->navegacao_anos();
        $this->load->view('post', $data); 
    }

    function comentar(){
        // $nome = $this->input->post('nome');
        // $email = $this->input->post('email');
        // $id_blog = $this->input->post('id_blog');
        // $mensagem = $this->input->post('mensagem');
        $back = $this->input->post('back');
        // if($this->db->get_where('blog', array('id' => $id_blog))->num_rows()){
        //     $this->db->set('id_blog', $id_blog)
        //              ->set('nome', $nome)
        //              ->set('email', $email)
        //              ->set('comentario', $mensagem)
        //              ->set('data', Date('Y-m-d'))
        //              ->set('datetime', Date('Y-m-d H:I:s'))
        //              ->insert('blog_comentarios');
        // }
        // $this->session->set_userdata('envio_comentario', TRUE);
        redirect($back);
    }

}