<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
		$this->load->model('blog_model', 'blog');
		$this->menuvar['categorias'] = $this->blog->navegacao_categorias();
        $this->menuvar['anos'] = $this->blog->navegacao_anos();
	}

	function index(){
		$data['contato'] = $this->db->get('contato')->result();
		$this->load->view('contato', $data);
	}

	function enviar(){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';

        $emailconf['protocol'] = 'mail';

        $emailconf['smtp_host'] = 'smtp.designfestaseflores.com.br';
        $emailconf['smtp_user'] = 'noreply@designfestaseflores.com.br';
        $emailconf['smtp_pass'] = 'noreplydesign1';
        $emailconf['smtp_port'] = 465;

        $emailconf['crlf'] = "\r\n";
        $emailconf['newline'] = "\r\n";

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_tel = $this->input->post('telefone');
        $u_msg = $this->input->post('mensagem');


        $from = 'noreply@designfestaseflores.com.br';
        $fromname = 'Design Festas e Flores';
        $to = 'contato@designfestaseflores.com.br';
        $bc = FALSE;
        $bcc = 'bruno@trupe.net';

        $assunto = 'Contato';
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:14px;
                                color:#5DB8C9;
                                font-family:Arial;
                            }
                            .val{
                                color:#000;
                                font-size:12px;
                                font-family:Arial;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br />
                    <span class='tit'>Telefone :</span> <span class='val'>$u_tel</span><br />
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_msg</span><br />
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);

        if($bc)
            $this->email->bc($bc);
        if($bcc)
            $this->email->bcc($bcc);

        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        $this->session->set_flashdata('envio_status', TRUE);

        $this->email->send();

		redirect('contato/index', 'refresh');
	}

}
