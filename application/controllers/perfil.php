<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
		$this->load->model('blog_model', 'blog');
		$this->menuvar['categorias'] = $this->blog->navegacao_categorias();
        $this->menuvar['anos'] = $this->blog->navegacao_anos();
	}

	function index(){
		$data['registro'] = $this->db->get('perfil')->result();

		$this->load->view('perfil', $data);
	}

}
