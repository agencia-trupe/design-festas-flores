<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('usuarios_model', 'usuarios');
   }

   function index($pag = 0){
   	$this->load->library('pagination');

      $pag_options = array(
         'base_url' => base_url("painel/usuarios/index/"),
         'per_page' => 20,
         'uri_segment' => 4,
         'next_link' => "Próxima →",
         'next_tag_open' => "<li class='next'>",
         'next_tag_close' => '</li>',
         'prev_link' => "← Anterior",
         'prev_tag_open' => "<li class='prev'>",
         'prev_tag_close' => '</li>',
         'display_pages' => TRUE,
         'num_links' => 10,
         'first_link' => FALSE,
         'last_link' => FALSE,
         'num_tag_open' => '<li>',
         'num_tag_close' => '</li>',
         'cur_tag_open' => '<li><b>',
         'cur_tag_close' => '</b></li>',
         'total_rows' => $this->usuarios->numeroResultados()
      );

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

      $data['registros'] = $this->usuarios->pegarPaginado($pag_options['per_page'], $pag);

      $data['titulo'] = 'Usuários';
      $data['unidade'] = "Usuário";
      $data['campo_1'] = "Usuário";
      $data['campo_2'] = "E-mail";

      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;
     
      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;

   	$this->load->view('painel/usuarios/lista', $data);
   }

   function form($id = false){
      if($id){
         $data['titulo'] = 'Editar Usuário';
   	   $data['registro'] = $this->usuarios->pegarPorId($id);
         if(!$data['registro'])
            redirect('painel/usuarios');
      }else{
         $data['titulo'] = 'Inserir Usuário';
         $data['registro'] = FALSE;
      }

      $data['unidade'] = "Usuário";
   	$this->load->view('painel/usuarios/form', $data);
   }

   function inserir(){
      if($this->usuarios->inserir()){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário inserido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir usuário');
      }

   	redirect('painel/usuarios/index', 'refresh');
   }

   function alterar($id){
      if($this->usuarios->alterar($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário alterado com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar usuário');
      }     
   	redirect('painel/usuarios', 'refresh');
   }

   function excluir($id){
	   if($this->usuarios->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
      }

      if($this->session->userdata('id') == $id)
         redirect('painel/home/logout');

   	redirect('painel/usuarios', 'refresh');
   }

}