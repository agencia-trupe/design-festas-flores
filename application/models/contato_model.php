<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'contato';

		$this->dados = array('telefone1', 'telefone2');
		$this->dados_tratados = array();
	}

}