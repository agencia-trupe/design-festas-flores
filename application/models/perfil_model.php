<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'perfil';

		$this->dados = array('texto', 'imagem1', 'imagem2', 'imagem3', 'imagem4', 'imagem5', 'imagem6', 'imagem7', 'imagem8', 'imagem_maior');
		$this->dados_tratados = array(
			'imagem1' => $this->sobeImagem('userfile1'),
			'imagem2' => $this->sobeImagem('userfile2'),
			'imagem3' => $this->sobeImagem('userfile3'),
			'imagem4' => $this->sobeImagem('userfile4'),
			'imagem5' => $this->sobeImagem('userfile5'),
			'imagem6' => $this->sobeImagem('userfile6'),
			'imagem7' => $this->sobeImagem('userfile7'),
			'imagem8' => $this->sobeImagem('userfile8'),
			'imagem_maior' => $this->sobeImagem('userfile9')
		);
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}

			if ($this->input->post('remover_imagem1') == '1')
				$this->db->set('imagem1', '');

			if ($this->input->post('remover_imagem2') == '1')
				$this->db->set('imagem2', '');

			if ($this->input->post('remover_imagem3') == '1')
				$this->db->set('imagem3', '');

			if ($this->input->post('remover_imagem4') == '1')
				$this->db->set('imagem4', '');

			if ($this->input->post('remover_imagem5') == '1')
				$this->db->set('imagem5', '');

			if ($this->input->post('remover_imagem6') == '1')
				$this->db->set('imagem6', '');

			if ($this->input->post('remover_imagem7') == '1')
				$this->db->set('imagem7', '');

			if ($this->input->post('remover_imagem8') == '1')
				$this->db->set('imagem8', '');

			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function sobeImagem($campo){

		$dir = '_imgs/perfil/';

		$uploadconfig = array(
		  'upload_path' => $dir,
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($dir.$arquivo['file_name'] , $dir.$filename);
		        	
		        	if ($campo == 'userfile9') {
		        		$this->image_moo
							->load($dir.$filename)
							->resize_crop(335, 335)
							->save($dir.$filename, TRUE);
		        	}else{
			          	$this->image_moo
							->load($dir.$filename)
							->resize(900, 900)
							->save($dir.$filename, TRUE)
							->resize_crop(163, 163)
							->save($dir."thumbs/".$filename, TRUE);
		        	}
		        return $filename;
		    }
		}else{
		    return false;
		}
	}	

}