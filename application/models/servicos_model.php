<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'servicos';

		$this->dados = array('texto', 'imagem1', 'imagem2', 'imagem3', 'imagem4');
		$this->dados_tratados = array(
			'imagem1' => $this->sobeImagem('userfile1'),
			'imagem2' => $this->sobeImagem('userfile2'),
			'imagem3' => $this->sobeImagem('userfile3'),
			'imagem4' => $this->sobeImagem('userfile4')
		);
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}

			if ($this->input->post('remover_imagem1') == '1')
				$this->db->set('imagem1', '');

			if ($this->input->post('remover_imagem2') == '1')
				$this->db->set('imagem2', '');

			if ($this->input->post('remover_imagem3') == '1')
				$this->db->set('imagem3', '');

			if ($this->input->post('remover_imagem4') == '1')
				$this->db->set('imagem4', '');

			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function sobeImagem($campo){

		$dir = '_imgs/servicos/';

		$uploadconfig = array(
		  'upload_path' => $dir,
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($dir.$arquivo['file_name'] , $dir.$filename);
		        
		          	$this->image_moo
						->load($dir.$filename)
						->resize(900, 900)
						->save($dir.$filename, TRUE)
						->resize_crop(163, 163)
						->save($dir."thumbs/".$filename, TRUE);
		        
		        return $filename;
		    }
		}else{
		    return false;
		}
	}	

}