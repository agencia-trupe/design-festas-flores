<section>

	<h1 class="titulo-pagina">perfil</h1>

	<div class="w-bg">

		<img src="_imgs/perfil/<?=$registro[0]->imagem_maior?>" class="ampliada">

		<div class="texto">
			<?=$registro[0]->texto?>
		</div>

		<div class="barra">
			<h2>Design Festas & Flores</h2>
			<h1>Muito carinho e dedicação em todas as peças</h1>
			<a href="http://www.facebook.com/designfestaseflores" target="_blank" title="Nossa página no facebook"><img src="_imgs/layout/compartilhar_facebook.png" alt="facebook coração"></a>
		</div>

		<?php if ($registro[0]->imagem1): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem1?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem1?>"></a>			
		<?php endif ?>

		<?php if ($registro[0]->imagem2): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem2?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem2?>"></a>			
		<?php endif ?>

		<?php if ($registro[0]->imagem3): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem3?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem3?>"></a>
		<?php endif ?>

		<?php if ($registro[0]->imagem4): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem4?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem4?>"></a>			
		<?php endif ?>

		<?php if ($registro[0]->imagem5): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem5?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem5?>"></a>			
		<?php endif ?>

		<?php if ($registro[0]->imagem6): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem6?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem6?>"></a>			
		<?php endif ?>

		<?php if ($registro[0]->imagem7): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem7?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem7?>"></a>			
		<?php endif ?>

		<?php if ($registro[0]->imagem8): ?>
			<a href="_imgs/perfil/<?=$registro[0]->imagem8?>" rel="galeria" class="fancy"><img src="_imgs/perfil/thumbs/<?=$registro[0]->imagem8?>"></a>			
		<?php endif ?>

	</div>

</section>
