<section>

	<div class="biglink">

		<div class="bg">

			<div id="barra-facebook">
				<div style="float:right" class="fb-like" data-href="<?=current_url()?>" data-send="false" data-layout="button_count" data-width="120" data-show-faces="false" data-font="tahoma"></div>
			</div>

			<div class="titulo">
				<h2><?=$post->titulo_categoria?></h2>
				<h1><?=$post->titulo?></h1>

				<div class="data">
					<?=$post->data_formatada?>
				</div>
			</div>

			<div class="texto">
				<?=$post->texto?>
			</div>

			<?php if ($post->imagens): ?>
				<?php foreach ($post->imagens as $c => $img): ?>
					<div class="img">
						<img src="_imgs/blog/<?=$img->imagem?>">
						<div class="legenda"><?=$img->legenda?></div>
					</div>
				<?php endforeach ?>
			<?php endif ?>

		</div>

	</div>

	
	<div id="comentarios" style="display:none;">
		<div class="titulo">Comentários <div class="contagem"><?=$post->num_comentarios?></div></div>
		<?php if ($post->comentarios): ?>
			<?php foreach ($post->comentarios as $key => $value): ?>
				<div class="comentario">
					<div class="identificacao">
						<span class="nome"><?=$value->nome?></span><br>
						<?=dt2horas($value->datetime)?>
					</div>
					<div class="texto">
						<?=$value->comentario?>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>	
	
	

	<div id="comentar" style="display:none;">
		<div class="titulo">Novo comentário</div>
		<form action="home/comentar/" method="post" id="coment-form">
			<div class="coluna">
				<input type="text" name="nome" placeholder="nome" required>
				<input type="email" name="email" placeholder="e-mail" required>
				<input type="submit" value="ENVIAR &raquo;">
				<input type="hidden" name="id_blog" value="<?=$post->id?>">
				<input type="hidden" name="back" value="<?=current_url()?>">
			</div>
			<div class="coluna maior">
				<textarea name="mensagem" placeholder="mensagem" required></textarea>
			</div>
		</form>
	</div>

	<?php
	if (base_url($this->session->userdata('redirect')) == current_url()) {
		$vaaai = "javascript: window.history.back();";
	}else{
		$vaaai = $this->session->userdata('redirect');
	}
	?>

	<div id="voltar"><a href="<?=$vaaai?>" title="voltar">&laquo; voltar</a></div>

</section>
