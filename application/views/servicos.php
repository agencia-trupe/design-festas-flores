<section>

	<h1 class="titulo-pagina">serviços</h1>

	<div class="w-bg">
		<div class="a-bg">
			<div class="texto"><?=$registro[0]->texto?></div>
			<div class="imagens">

				<?php if ($registro[0]->imagem1): ?>
					<a href="_imgs/servicos/<?=$registro[0]->imagem1?>" class="fancy" rel="galeria"><img src="_imgs/servicos/thumbs/<?=$registro[0]->imagem1?>"></a>
				<?php endif ?>

				<?php if ($registro[0]->imagem2): ?>
					<a href="_imgs/servicos/<?=$registro[0]->imagem2?>" class="fancy" rel="galeria"><img src="_imgs/servicos/thumbs/<?=$registro[0]->imagem2?>"></a>
				<?php endif ?>

				<?php if ($registro[0]->imagem3): ?>
					<a href="_imgs/servicos/<?=$registro[0]->imagem3?>" class="fancy" rel="galeria"><img src="_imgs/servicos/thumbs/<?=$registro[0]->imagem3?>"></a>
				<?php endif ?>

				<?php if ($registro[0]->imagem4): ?>
					<a href="_imgs/servicos/<?=$registro[0]->imagem4?>" class="fancy" rel="galeria"><img src="_imgs/servicos/thumbs/<?=$registro[0]->imagem4?>"></a>
				<?php endif ?>

			</div>
		</div>
		<div class="barra">
			<h2>Design Festas & Flores</h2>
			<h1>Muito carinho e dedicação em todas as peças</h1>
			<a href="http://www.facebook.com/designfestaseflores" target="_blank" title="Nossa página no facebook"><img src="_imgs/layout/compartilhar_facebook.png" alt="facebook coração"></a>
		</div>
	</div>

</section>