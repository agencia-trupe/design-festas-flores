<div class="centro">

	<header>

		<img src="_imgs/layout/logo.png" alt="Design Festas & Flores" id="logo">

		<a href="http://www.facebook.com/designfestaseflores" target="_blank" title="Nossa página no facebook" id="fb-link"><img src="_imgs/layout/facebook.png" alt="facebook coração"></a>

		<nav>
			<ul>
				<li <?if($this->router->class=='home')echo" class='parent-ativo'"?> id="mn-home"><a href="home" title="Página Inicial" <?if($this->router->class=='home')echo" class='ativo'"?>>home</a></li>
				<li <?if($this->router->class=='perfil')echo" class='parent-ativo'"?> id="mn-perfil"><a href="perfil" title="Nossa Empresa" <?if($this->router->class=='perfil')echo" class='ativo'"?>>perfil</a></li>
				<li <?if($this->router->class=='servicos')echo" class='parent-ativo'"?> id="mn-servicos"><a href="servicos" title="Nossos Serviços" <?if($this->router->class=='servicos')echo" class='ativo'"?>>serviços</a></li>
				<li <?if($this->router->class=='contato')echo" class='parent-ativo'"?> id="mn-contato"><a href="contato" title="Entre em contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>contato</a></li>
			</ul>		
		</nav>

		<p id="frase-topo">
			Design Festas e Flores e uma empresa especializada em lembranças e presentes para casamentos, aniversários, batizados, entre outros, oferecendo um serviço personalizado, criativo e confeccionado com qualidade, para que todos os detalhes do seu evento sejam lembrados com todo carinho.
		</p>
		
	</header>

	<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

		<aside>
			<ul id="lista-categorias">
				<?php foreach ($categorias as $key => $value): ?>
					<li><?=$value?></li>
				<?php endforeach ?>
			</ul>
			<ul id="lista-anos">
				<li class="titulo">ARQUIVOS</li>
				<?php foreach ($anos as $key => $value): ?>
					<li><?=$value?></li>
				<?php endforeach ?>
			</ul>
		</aside>