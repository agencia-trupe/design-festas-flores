
      </div> <!-- fim da div main -->
    </div> <!-- fim da div centro -->
  </div> <!-- fim da div polka -->

  <footer>

    <div class="centro">

      <ul>
        <li class="link-home"><a href="home" title="Página Inicial">&raquo; HOME</a></li>
        <li class="link-perfil"><a href="perfil" title="Nossa Empresa">&raquo; PERFIL</a></li>
        <li class="link-servicos"><a href="servicos" title="Nossos Serviços">&raquo; SERVIÇOS</a></li>
        <li class="link-contato"><a href="contato" title="Entre em contato">&raquo; CONTATO</a></li>
      </ul>

      <div class="fright">

        <div id="telefones">
          <?=$contato->telefone1?><br>
          <?=$contato->telefone2?>
        </div>

        <div id="assinatura">
          &copy; <?=date('Y')?> Design Festas & Flores - by Jô Fidelis<br>
          Todos os direitos reservados<br>
          <a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa"><span>Criação de Sites : Trupe Agência Criativa</span> <br> <img src="_imgs/layout/trupe.png"></a>
        </div>

      </div>

    </div>
  
  </footer>
  
  
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('fancybox','front.min'))?>
  
</body>
</html>
