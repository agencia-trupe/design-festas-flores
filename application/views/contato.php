<section>

	<h1 class="titulo-pagina">contato</h1>

	<div class="w-bg">

		<?php if ($this->session->flashdata('envio_status')): ?>
			<div class="retorno">
				Obrigado pelo contato!<br> Responderemos assim que possível.
			</div>	
		<?php endif ?>

		<div class="contato">

			<div class="telefone">
				<?=$contato[0]->telefone1?><br>
				<?=$contato[0]->telefone2?>
			</div>

			<div class="form">
				<strong>Fale conosco</strong><br>
				e solicite um orçamento!

				<form method="post" action="contato/enviar" id="form-contato">

					<input type="text" name="nome" placeholder="nome" required id="input-nome">

					<input type="email" name="email" placeholder="e-mail" required id="input-email">

					<input type="text" name="telefone" placeholder="telefone" required id="input-telefone">

					<textarea name="mensagem" required id="input-mensagem" placeholder="mensagem"></textarea>

					<input type="submit" value="ENVIAR &raquo;">

				</form>
			</div>

		</div>

		<div class="barra">
			<h2>Design Festas & Flores</h2>
			<h1>Muito carinho e dedicação em todas as peças</h1>
			<a href="http://www.facebook.com/designfestaseflores" target="_blank" title="Nossa página no facebook"><img src="_imgs/layout/compartilhar_facebook.png" alt="facebook coração"></a>
		</div>
	</div>

</section>
