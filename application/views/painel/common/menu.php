<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container">

      <a href="painel/home" class="brand">Design Festas & Flores</a>

      <ul class="nav">

        <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

        <li class="dropdown  <?if($this->router->class=='blog')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/blog/categorias">Categorias</a></li>
            <li><a href="painel/blog">Posts</a></li>
          </ul>
        </li>

        <li <?if($this->router->class=='perfil')echo" class='active'"?>><a href="painel/perfil">Perfil</a></li>

        <li <?if($this->router->class=='servicos')echo" class='active'"?>><a href="painel/servicos">Serviços</a></li>

        <li <?if($this->router->class=='contato')echo" class='active'"?>><a href="painel/contato">Contato</a></li>

        <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/usuarios">Usuários</a></li>
            <li><a href="painel/home/logout">Logout</a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>
</div>