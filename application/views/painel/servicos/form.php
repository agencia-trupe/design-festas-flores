<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
	<h2>
	  <?=$titulo?>
	</h2>
  </div>

<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

	<div id="dialog"></div>
	
	<label>Texto da Seção Serviços
	<textarea name="texto" class="medio completo"><?=$registro->texto?></textarea></label> <br><br>

	Imagem 1
	<?php if ($registro->imagem1): ?>
	  <br><img src="_imgs/servicos/<?=$registro->imagem1?>" style="max-width:300px"><br>
	  <label><input type="checkbox" name="remover_imagem1" value="1"> Remover Imagem</label>
	<?php endif ?>
	<label><input type="file" name="userfile1"></label><br><br>

	Imagem 2
	<?php if ($registro->imagem2): ?>
	  <br><img src="_imgs/servicos/<?=$registro->imagem2?>" style="max-width:300px"><br>
	  <label><input type="checkbox" name="remover_imagem2" value="1"> Remover Imagem</label>
	<?php endif ?>
	<label><input type="file" name="userfile2"></label>	<br><br>

	Imagem 3
	<?php if ($registro->imagem3): ?>
	  <br><img src="_imgs/servicos/<?=$registro->imagem3?>" style="max-width:300px"><br>
	  <label><input type="checkbox" name="remover_imagem3" value="1"> Remover Imagem</label>
	<?php endif ?>
	<label><input type="file" name="userfile3"></label>	<br><br>

	Imagem 4
	<?php if ($registro->imagem4): ?>
	  <br><img src="_imgs/servicos/<?=$registro->imagem4?>" style="max-width:300px"><br>
	  <label><input type="checkbox" name="remover_imagem4" value="1"> Remover Imagem</label>
	<?php endif ?>
	<label><input type="file" name="userfile4"></label>	<br><br>

	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Salvar</button>
		<button class="btn btn-voltar" type="reset">Voltar</button>
	</div>

</form>  