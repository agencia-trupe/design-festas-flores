<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
	<h2>
	  <?=$titulo?>
	</h2>
  </div>

<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

	<div id="dialog"></div>
	
	<label>Telefone 1
	<input type="text" name="telefone1" value="<?=$registro->telefone1?>"></label>

	<label>Telefone 2
	<input type="text" name="telefone2" value="<?=$registro->telefone2?>"></label>

	<div class="form-actions">
		<button class="btn btn-primary" type="submit">Salvar</button>
		<button class="btn btn-voltar" type="reset">Voltar</button>
	</div>

</form>  