<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed">

          <thead>
            <tr>
              <!-- <th>Ordenar</th> -->
              <th class="yellow header headerSortDown">Telefone 1</th>
              <th class="yellow header headerSortDown">Telefone 2</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <td><?=$value->telefone1?></td>
                  <td><?=$value->telefone2?></td>
                  <td class="crud-actions" style="width:54px;">
                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>                    
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php endif ?>

    </div>
  </div>