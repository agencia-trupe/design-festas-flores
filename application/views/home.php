<section>

	<?php if ($registros): ?>
		
		<?php foreach ($registros as $key => $value): ?>

			<a class="biglink" href="home/detalhes/<?=$value->slug?>" title="<?=$value->titulo?>">

				<div class="bg">

					<?php if ($value->imagens): ?>
						<?php foreach ($value->imagens as $c => $img): ?>
							<?php if ($c == 0): ?>
								<img src="_imgs/blog/<?=$img->imagem?>" class="ampliada">
							<?php else: ?>
								<img src="_imgs/blog/thumbs/<?=$img->imagem?>" class="thumbs  <?if($c == 1)echo" primeira"?> <?if($c == 5)echo" ultima"?>">
							<?php endif ?>
							 <?if($c == 5) break;?>
						<?php endforeach; ?>
					<?php endif ?>

					<div class="titulo">

						<h2><?=$value->titulo_categoria?></h2>
						<h1><?=$value->titulo?></h1>

						<div class="data">
							<?=$value->data_formatada?>
						</div>

					</div>

				</div>

				<div class="no-bg">
					<span style="display:none;"><?=$value->num_comentarios?> &bull;</span> Clique aqui para Curtir/Compartilhar
				</div>

			</a>
			
		<?php endforeach ?>

		<?php if ($paginacao): ?>
			<div id="paginacao">
				<?=$paginacao?>
			</div>
		<?php endif ?>

	<?php else: ?>

		<h2 class="sem-resultados">Nenhum post encontrado</h2>
		
	<?php endif ?>

</section>